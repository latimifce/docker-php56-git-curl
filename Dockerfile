FROM nmcteam/php56

MAINTAINER Átila Camurça, Samir Coutinho

RUN apt-get update -qq && \
    apt-get -qq install -y git curl && \
    apt-get -qq clean
